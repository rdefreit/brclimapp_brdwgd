output$about_plot <- renderPlotly({

  source("database_connection.R", local = TRUE)
  
  code_muni_rnd <- brmun %>%
    sample_n(size = 1)
  
  cod7 <- code_muni_rnd$code_muni
  
  res_tmax <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{tmax}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'Tmax_mean')")
  ) %>%
    mutate(name = "Tmax_mean")
  
  res_tmin <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{tmin}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'Tmin_mean')")
  ) %>%
    mutate(name = "Tmin_mean")
  
  res_pr <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{pr}' WHERE (code_muni = {cod7} AND date >= '{date_start}')  AND name = 'pr_sum'")
  )
  
  dbDisconnect(conn, shutdown = TRUE)
  
  p1 <- ggplot(data = bind_rows(res_tmax, res_tmin), aes(x = date, y = value, color = name)) +
    geom_line(alpha = .5) +
    # geom_smooth() +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      title = paste0(code_muni_rnd$name_muni, ", ", code_muni_rnd$abbrev_state),
      x = "Date", 
      y = "Average temperature",
      color = ""
    ) 
  
  p2 <- ggplot(data = res_pr, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#268bd2") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Precipitation (sum)",
      color = ""
    ) +
    theme(legend.position = "none")
  
  pp1 <- ggplotly(p1, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp2 <- ggplotly(p2, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  
  subplot(pp1, pp2, nrows = 2, heights = c(.7, .3), shareX = TRUE, titleY = TRUE) %>%
    # layout(
    #   # legend = list(x = 0.01, y = 0.01),
    #   xaxis = list(rangeslider = list(type = "date"))
    # ) %>%
    config(displayModeBar = FALSE) %>%
    layout(hovermode = "x unified")
  
})
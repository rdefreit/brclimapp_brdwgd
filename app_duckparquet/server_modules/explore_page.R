updateSelectizeInput(
  session = session, 
  inputId = "select_mun", 
  choices = mun_lista, 
  server = TRUE
)

output$explore_plot <- renderPlotly({
  req(input$select_mun)
  
  cod7 <- input$select_mun
  
  source("database_connection.R")
  
  res_tmax <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{tmax}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'Tmax_mean')")
  ) %>%
    mutate(name = "Tmax_mean")
  
  res_tmin <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{tmin}' WHERE (code_muni = {cod7} AND date >= '{date_start}') AND name = 'Tmin_mean'")
  ) %>%
    mutate(name = "Tmin_mean")
  
  res_pr <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{pr}' WHERE (code_muni = {cod7} AND date >= '{date_start}') AND name = 'pr_sum'")
  )
  
  res_eto <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{eto}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'ETo_sum')")
  )
  
  res_rh <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{rh}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'RH_mean')")
  )
  
  res_rs <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{rs}' WHERE (code_muni = {cod7} AND date >= '{date_start}' AND name = 'Rs_mean')")
  )
  
  res_u2 <- dbGetQuery(
    conn, 
    glue("SELECT date, value FROM '{u2}' WHERE (code_muni = {cod7} AND date >= '{date_start}') AND name = 'u2_mean'")
  )
  
  dbDisconnect(conn, shutdown=TRUE)
  
  p1 <- ggplot(data = bind_rows(res_tmax, res_tmin), aes(x = date, y = value, color = name)) +
    geom_line(alpha = .5) +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      title = names(mun_lista[mun_lista==input$select_mun]),
      x = "Date", 
      y = "Temperature",
      color = ""
    ) 
  
  p2 <- ggplot(data = res_pr, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#268bd2") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Precipitation",
      color = ""
    )
  
  p3 <- ggplot(data = res_eto, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#2aa198") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Evapotranspiration",
      color = ""
    )
  
  p4 <- ggplot(data = res_rh, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#b58900") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Relative humidity",
      color = ""
    )
  
  p5 <- ggplot(data = res_rs, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#cb4b16") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Solar radiation",
      color = ""
    )
  
  p6 <- ggplot(data = res_u2, aes(x = date, y = value)) +
    geom_line(alpha = .5, color = "#d33682") +
    scale_x_date(date_breaks = "5 year", date_labels =  "%Y") +
    ylim(0, NA) +
    labs(
      x = "Date", 
      y = "Wind speed",
      color = ""
    )
  
  pp1 <- ggplotly(p1, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp2 <- ggplotly(p2, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp3 <- ggplotly(p3, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp4 <- ggplotly(p4, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp5 <- ggplotly(p5, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  pp6 <- ggplotly(p6, dynamicTicks = TRUE) %>% layout(yaxis = list(autorange = FALSE))
  
  subplot(pp1, pp2, pp3, pp4, pp5, pp6, 
          nrows = 6, 
          shareX = TRUE, titleY = TRUE, margin = c(.01,0,0,.01)) %>%
    config(displayModeBar = TRUE) %>%
    layout(hovermode = "x unified") 
  
})
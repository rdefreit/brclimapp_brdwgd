FROM openanalytics/r-ver

LABEL maintainer="Raphael Saldanha <rfsaldanha@gmail.com>"

# system libraries of general use
RUN apt-get update && apt-get install --no-install-recommends -y \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# install dependencies
RUN R -q -e "install.packages(c('shiny', 'dplyr', 'dbplyr', 'duckdb', 'bslib', 'thematic', 'ragg', 'plotly', 'shinycssloaders', 'glue'))"

# copy the app to the image
RUN mkdir /root/app
COPY app_duckparquet /root/app

CMD ["R", "-q", "-e", "shiny::runApp('/root/app', host='0.0.0.0', port=3838)"]

EXPOSE 3838
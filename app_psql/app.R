
# Packages ----------------------------------------------------------------

library(shiny)
library(dplyr)
library(RPostgres)
library(bslib)
library(thematic)
library(ragg)
library(plotly)
library(shinycssloaders)
options(shiny.useragg = TRUE)
thematic_shiny(font = "auto")


# Data load ---------------------------------------------------------------

# Municipality codes and names
brmun <- readRDS(file = "data/brmun_ngeom.rds") %>%
  arrange(name_muni)
mun_lista <- as.list(brmun$code_muni)
names(mun_lista) <- paste0(brmun$name_muni, ", ", brmun$abbrev_state)


# UI ----------------------------------------------------------------------

ui <- tagList(
  tags$head(
    tags$style(HTML(
      "html {
             position: relative;
             min-height: 100%;
           }
           body {
             margin-bottom: 110px; /* Margin bottom by footer height */
           }
           .footer {
             position: absolute;
             bottom: 0;
             width: 90%;
             height: 110px; /* Set the fixed height of the footer here */
           }"))),
  navbarPage(
    theme = bs_theme(bootswatch = "solar"),
    inverse = TRUE,
    title = "brclim",
    tabPanel(
      title = "About",
      sidebarLayout(
        mainPanel(
          width = 5,
          fluidPage(
            h2("Zonal climatic indicators for Brazilian municipalities, from 1963-01-01 to 2020-07-31"),
            p("Zonal indicators (mean, standard deviation, maximum, minimum and sum for volume variables) were calculated for the entire municipality area, urbanized areas and non-urbanized areas accordingly to IBGE."),
            p("The Brazilian Daily Weather Gridded Data (BRDWGD) from ", a(href = 'https://doi.org/10.1002/joc.7731', 'Xavier et al. 2022', .noWS = "outside", target = "_blank"), " was used as the reference dataset, containing maximum and minimum temperature (°C), precipitation (mm), evapotranspiration (mm), solar radiation (MJ/m2), wind speed (m/s), and relative humidity (%)."),
            p("The graphs here present data area after 2000-01-01 due speed and space limitations. The full dataset with all years, zonal statistics and climatic variables are available to download."),
            tags$cite("How quote this project")
          )
        ),
        mainPanel(
          width = 7,
          fluidPage(
            plotlyOutput(outputId = "about_plot", height = 600) %>% withSpinner(type = 8, color = "#2aa198"),
            tags$small(p(style="text-align:center", "Random municipality choosen from the database."))
          )
        )
      )
    ),
    tabPanel(
      title = "Explore",
      fluidPage(
        fluidRow(
          column(
            width = 4,
            selectInput(inputId = "select_mun", label = "Municipality", choices = NULL)
          ),
          column(
            width = 8,
            p("The graph bellow present average values for maximum and minimum temperature (°C), relative humidity (%), solar radiation (MJ/m2) and wind speed (m/2), and sum values for precipitation (mm) and evapotranspiration (mm)."),
            p("Datasets with others zonal statistics (maximum, minimum and standard deviation) for all indicators and data since 1963 are available for download.")
          )
        ),
        fluidRow(
          plotlyOutput(outputId = "explore_plot", height = 800) %>% withSpinner(type = 8, color = "#2aa198")
        )
      )
    ),
    tabPanel(
      title = "Datasets"
    )
  ),
  tags$footer(
    class = "footer",
    fluidRow(
      column(width = 3),
      column(width = 3, HTML('<center><img src="inria.png" width = "150"></center>')),
      column(width = 3, HTML('<center><img src="lncc.png" width = "150"></center>')),
      column(width = 3)
    )
  )
)




# Server ------------------------------------------------------------------

server <- function(input, output, session) {
  ### About page
  source(file = "server_modules/about_plot.R", local = TRUE)
  
  ### Explore page
  source(file = "server_modules/explore_page.R", local = TRUE)
  
}

# Run the application 
shinyApp(ui = ui, server = server)
